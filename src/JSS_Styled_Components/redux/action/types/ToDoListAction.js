import {
  ADD_TASK_TO_DO,
  DELETE_TASK,
  DONE_TASK,
  EDIT_TASK,
  SELECT_THEME,
} from "../../contant/toDoListContant";

export const addTaskAction = (newTask) => ({
  type: ADD_TASK_TO_DO,
  payload: newTask,
});

export const selectTheme = (theme) => ({
  type: SELECT_THEME,
  payload: theme,
});

export const doneTask = (taskID) => ({
  type: DONE_TASK,
  payload: taskID,
});

export const deleteTask = (taskID) => ({
  type: DELETE_TASK,
  payload: taskID,
});

export const editTask = (task) => ({
  type: EDIT_TASK,
  payload: task,
});
