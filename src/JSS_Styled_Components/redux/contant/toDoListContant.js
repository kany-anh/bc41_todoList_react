export const ADD_TASK_TO_DO = "ADD_TASK_TO_DO";
export const SELECT_THEME = "SELECT_THEME";
export const DONE_TASK = "DONE_TASK";
export const DELETE_TASK = "DELETE_TASK";
export const EDIT_TASK = "EDIT_TASK";
